# Router.js


## isUnsafe

### Name
**isUnsafe** - Verifies if a char belongs to a invalid charset

### Sinopsis
bool isUnsafe (char c)

### Description
Verifies if a given char is a invalid char. An valid char is every char
of the UTF-16 encoding, except for the ones lower than 33 or bigger than
122, and the ones of following list, comma separated : ", <, >, %, \, ^,
[, ], `, +, $, ,, ', #, &;

The function returns true (it is a invalid char), or false.

## decToHex

### Name
decToHex - Convert a number in decmal to it's correpondant, in hexadecimal.

### Sinopsis
string decToHex (int n)

### Description
Makes the direct conversion of a given integer to its equivalent in
hexadecimal. All hex chars are
stored in an array called hexVals. The operation, then, starts as the
handmade conversion, where the given number is divided by 16. The rest is
used to choose one of the hexVals chars, wich is concatenated to the final
string. The number is floored, and the operation follows. When the number
gets lower than 16, the last hexVals is choosed based on the remaining of
the number. Finally, returns the reverse of the formed string.

## reversal

### Name
reversal - Reveses a string.

### Sinopsis
string reverse (string s)

### Description
Given a string, it loops backwards taking the last chars of the string, and
saving first in a new variable. Then returns the new variable.

## convert

### Name
convert - A hex marker

### Sinopsis
string convert (integer n)

### Description
When a to-be converted number is passed as a argument, it returns the
converted number, preceded by a % as a hex marker.

## encodeUrl

### Name
encodeUrl - Encode the given string removing all invalid chars, taking
account the unicode table.

### Sinopsis
string encodeUrl (string s)

### Description
For each character inside the string s, it verifies if the text is valid.
It is, if at first, every letter is lower than the 255 letter of unicode.
Then uses the remaining unsafe chars. If there is any letter bigger than
254, it returns a warning of invalid char.

## onlogin 

### Name
onlogin - Returns the evalueated string of login.

### Sinopsis
string onlogin ()

### Description
Returns a evaluated string, composed by the following pattern:

pattern = 'location="login.cgi@username="enc_user"&psd="enc_pass""'

where enc_user, and enc_pass are, respectivelly, the return of encodeUrl,
called with username.value and password.value as argument.

As the script has just one form, it's only index is 0. If a username is
given, it follows by creating the url to be evalueated, removing invalid
chars from the url: it will contain username and password attached to it.

Then returns the evaluation of the pattern:

eval ('pattern');

The function might have a bug: using the "with" statement, if either
username or password are not defined, the function still runs, with
undefined values and behavior.


