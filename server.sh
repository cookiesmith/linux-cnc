#!/bin/bash

action()
{
	for $i in $CLIENTS; do
}

get_tasklist()
{
	cp $WORKDIR/"$CLIENT"/"$TASKLIST" "$WORKDIR"
}

self_prepare()
{
	if [ $1 -eq 1 ]; then
		cp /etc/hosts /etc/hosts.old
		echo -e "$CLIENT\t$DNS$" >> "$WORKDIR/hosts.tail"
		
	elif [ $1 -eq 0 ]; then
		mv /etc/hosts.old /etc/hosts
	fi
	chmod 644 /etc/hosts /etc/hosts.old
}

command_handler()
{
	while [ $# -ne 0 ]; do
		case $1 in
			1)	# Send & execute tasklist, if available
				get_tasklist
				;;

			2)
				# Journal delivery
				mv journal $JOURNAL
				;;

			3)
				;;

			*)
				;;
		esac
		shift 2
	done
}

main()
{
	DEFAULT=200
	DNS=$1
	CLIENT=$2
	COMMAND=$3
	if [ -d $WORKDIR/$CLIENT ]; then
		source $WORKDIR/$CLIENT/.conf
	else
		source $WORKDIR/$DEFAULT/.conf
	fi

	self_prepare 1
	command_handler "$COMMAND"
	self_prepare 0
}

main $@
